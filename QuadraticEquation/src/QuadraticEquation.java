/* Third part of the 2nd lab. Calculates solutions
 * for the quadratic equation from user input
 * */

import java.lang.Math;
import comp102x.IO;
public class QuadraticEquation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		quadraticEquation();
	}

	public static void quadraticEquation() {
		System.out.print("Enter a: ");
		double a = IO.inputDouble();
		System.out.print("Enter b: ");
		double b = IO.inputDouble();
		System.out.print("Enter c: ");
		double c = IO.inputDouble();
		double discriminant = Math.sqrt(b*b - 4*a*c);
		double solution1 = (-b + discriminant)/(2.0 * a);
		double solution2 = (-b - discriminant)/(2.0 * a);
		System.out.println("First solution for x = " + Double.toString(solution1));
		System.out.println("Second solution for x = " + Double.toString(solution2));
	}
}
