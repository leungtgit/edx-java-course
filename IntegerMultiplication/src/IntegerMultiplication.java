/* First real lab in the EdX java course.
 * Makes use of the libraries provided by the course to
 * demonstrate simple IO. 
 * */

import comp102x.IO;
public class IntegerMultiplication {
	public static void main(String[] args) {
		multiply();
	}
	public static void multiply() {
		System.out.print("Enter an integer, x: ");
		int x = IO.inputInteger();
		System.out.print("Enter an integer, y: ");
		int y = IO.inputInteger();
		System.out.println("Answer = " + Integer.toString(x*y));
	}
}