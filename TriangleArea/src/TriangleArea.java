/* 2nd part of the 2nd lab in the EdX Java course.
 * It calculates the area from user input then outputs it.
 * */
import comp102x.IO;
public class TriangleArea {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		calculateTriangleArea();
	}

	public static void calculateTriangleArea() {
		System.out.print("Enter the width of the triangle: ");
		double width = IO.inputDouble();
		System.out.print("Enter the height of the triangle: ");
		double height = IO.inputDouble();
		System.out.print("The triangle area = " + Double.toString((width * height / 2.0)) );
	}
	
}
