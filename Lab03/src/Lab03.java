/**
 * Lab 3 of Introduction to Java Programming on EdX offered by HKUST.
 * This is a quick lab that demonstrates the ability to call on and use
 * functions offered by external jar files.
 * @author Torrance Leung
 * @version 2018-08-06
 */

import comp102x.ColorImage;
public class Lab03 {
	/**
	 *Loads an image onto a canvas. 
	 */
	public static void loadAnImage() {
		comp102x.ColorImage newImage = new comp102x.ColorImage();
		comp102x.Canvas newCanvas = new comp102x.Canvas(newImage.getWidth(), newImage.getHeight());
		newCanvas.add(newImage, 0, 0);
	}
	/**
	 * Loads two images onto a canvas.
	 */
	public static void loadTwoImagesOnTheSameCanvas() {
		//load images
		comp102x.ColorImage image1 = new comp102x.ColorImage();
		comp102x.ColorImage image2 = new comp102x.ColorImage();
		//find the biggest in the y direction
		int biggestHeight = 0;
		if(image1.getHeight() > image2.getHeight())
			biggestHeight = image1.getHeight();
		else
			biggestHeight = image2.getHeight();
		//add images to canvas
		comp102x.Canvas twoImageCanvas = new comp102x.Canvas(image1.getWidth() + image2.getWidth(),biggestHeight);
		twoImageCanvas.add(image1, 0, 0);
		twoImageCanvas.add(image2, image1.getWidth(), 0);
		
	}
	
	/**
	 * Adds two images together
	 */
	public static void imageAddition() {
		//load images
		comp102x.ColorImage image1 = new comp102x.ColorImage();
		comp102x.ColorImage image2 = new comp102x.ColorImage();
		
		
		//add both images together
		comp102x.ColorImage image3 = new comp102x.ColorImage(image1.getWidth(), image1.getHeight());
		image3 = ColorImage.add(image1, image2);
		
		//Create a canvas and draw the images
		comp102x.Canvas allImages = new comp102x.Canvas(image1.getWidth() * 3 + 20, image1.getHeight());
		allImages.add(image1, 0, 0);
		allImages.add(image2, image1.getWidth() + 10, 0);
		allImages.add(image3, image1.getWidth() * 2 + 20, 0);
		
	}
	
	/**
	 * Multiplies(?) two images. I have no idea. The function description
	 * says that it multiplies two ColorImage objects together. The one that
	 * multiplies the RGB values makes more sense. Maybe it multiplies the
	 * pixel values together.
	 */
	
	public static void imageMultiplication() {
		//load images
		comp102x.ColorImage image1 = new comp102x.ColorImage();
		comp102x.ColorImage image2 = new comp102x.ColorImage();
		
		//multiply the images
		comp102x.ColorImage image3 = new comp102x.ColorImage(image1.getWidth(), image1.getHeight());
		image3 = ColorImage.multiply(image1, image2);
		
		//create a canvas and draw the images
		comp102x.Canvas allImages = new comp102x.Canvas(image1.getWidth() * 3 + 20, image1.getHeight());
		allImages.add(image1, 0, 0);
		allImages.add(image2, image1.getWidth() + 10, 0);
		allImages.add(image3, image1.getWidth() * 2 + 20, 0);
		
	}
	
	/**
	 * Optional Task 1 - Changing Image Color
	 * Increments at intervals of 10.
	 */
	public static void changeColor() {
		comp102x.ColorImage imgToChangeColor = new comp102x.ColorImage();
		comp102x.Canvas colorImg = new comp102x.Canvas(imgToChangeColor.getWidth(), imgToChangeColor.getHeight());
		colorImg.add(imgToChangeColor);
		int userChoice = -1;
		while(userChoice != 0) {
			comp102x.IO.outputln("Enter a choice:");
			comp102x.IO.outputln("0. Exit");
			comp102x.IO.outputln("1. Increase red");
			comp102x.IO.outputln("2. Increase green");
			comp102x.IO.outputln("3. Increase blue");
			comp102x.IO.outputln("4. Decrease red");
			comp102x.IO.outputln("5. Decrease green");
			comp102x.IO.outputln("6. Decrease blue");
			comp102x.IO.outputln("7. Save");
			userChoice = comp102x.IO.inputInteger();
			
			switch(userChoice) {
			case 1:
				imgToChangeColor.increaseRed(10);
				break;
			case 2:
				imgToChangeColor.increaseGreen(10);
				break;
			case 3:
				imgToChangeColor.increaseBlue(10);
				break;
			case 4:
				imgToChangeColor.decreaseRed(10);
				break;
			case 5:
				imgToChangeColor.decreaseGreen(10);
				break;
			case 6:
				imgToChangeColor.decreaseBlue(10);
				break;
			case 7:
				imgToChangeColor.save();
			default:
				break;
				
			}
			colorImg.removeAll();
			colorImg.add(imgToChangeColor);
		}
	}
	
	/**
	 * This is the main function. It does things until you tell it to stop.
	 * Closing all popup windows seems to terminate the program. Do leave at least
	 * one open at all times. The program will terminate if you close the last window.
	 */
	public static void main(String[] args) {
		int userChoice = -1;
		while(userChoice != 0) {
			comp102x.IO.outputln("Enter a choice:");
			comp102x.IO.outputln("0. Exit");
			comp102x.IO.outputln("1. Load image");
			comp102x.IO.outputln("2. Load 2 images");
			comp102x.IO.outputln("3. Add 2 images together");
			comp102x.IO.outputln("4. Multiply 2 images together");
			comp102x.IO.outputln("5. Change R, G, or B by 10");
			userChoice = comp102x.IO.inputInteger();
			
			switch(userChoice) {
			case 1:
				loadAnImage();
				break;
			case 2:
				loadTwoImagesOnTheSameCanvas();
				break;
			case 3:
				imageAddition();
				break;
			case 4:
				imageMultiplication();
				break;
			case 5:
				changeColor();
			default:
				break;
				
			}
		}
		return;

	}
}
